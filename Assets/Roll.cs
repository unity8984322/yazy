using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;

namespace Yazzy
{
    public class Roll : MonoBehaviour
    {
        public static Roll instance;
        public Action ActionTodisablebuttons;


        [SerializeField] AIPlayer AIPlayer;
        [SerializeField] GameObject BackgroundImgObj;
        public GameObject counterTxt;
        public GameObject blockImg;
        [SerializeField] Button exitBtn;
        [SerializeField] Transform boardParent;
        [SerializeField] Transform objectToRotateView;
        [SerializeField] RectTransform rollDicePanel;
        [SerializeField] Transform scoreText;
        [SerializeField] Sprite DefaultDiceSprite;
        public Transform addScoreEffect;
        public Button rollDiceBtn;
        //[SerializeField] AudioClip audioToPlay;
        public AudioSource audioSrc;
        [SerializeField] float rollDuration;
        [SerializeField] float doScaleDuration = 0.3f;
        [SerializeField] float timeScale;
        [SerializeField] int i;

        public bool isFirstPlayerTurn;
        public static int chanceCounter;
        public bool vsComputer;
        public static bool hasFirstPlayerWon;

        public static Coroutine rollingCoroutine;

        [SerializeField] List<Sprite> animSprites;
        [SerializeField] List<Sprite> diceSprites;
        public List<GameObject> diceGameObjects;
        public List<Button> firstPlayerBtnList = new();
        public List<Button> secondPlayerBtnList = new();

        private void Start()
        {
            //DontDestroyOnLoad(this.gameObject);
            Application.targetFrameRate = 60;

            ActionTodisablebuttons += DisableAllButtons;

            if (instance == null)
                instance = this;

            AIPlayer = this.GetComponent<AIPlayer>();
            isFirstPlayerTurn = true;
        }

        public void RollDice()
        {
            audioSrc.mute = false;
            if (rollingCoroutine == null)
            {
                chanceCounter += 1;
                i = chanceCounter;
                Transform childIMg = rollDiceBtn.transform.GetChild(3).GetChild(chanceCounter - 1);
                childIMg.GetComponent<Image>().enabled = false;
                childIMg.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 0.5f);
                rollingCoroutine = StartCoroutine(RollDiceCoroutine());
            }
            else
            {
                Debug.Log("Coroutine wasn't null");
            }
        }

        private IEnumerator RollDiceCoroutine()
        {
            if (AddScoreBtn.count == 0 && vsComputer && !isFirstPlayerTurn)
                yield return new WaitForSeconds(2.1f);
            else if (AddScoreBtn.count != 0 && vsComputer && !isFirstPlayerTurn)
                yield return new WaitForSeconds(0.2f);

            if (vsComputer && !isFirstPlayerTurn)
                yield return new WaitForSeconds(0.3f);
            else if (!vsComputer)
                yield return new WaitForSeconds(0.1f);

            int res = 0;
            Debug.Log($"<color=Red> isFirstPlayerTurn: {isFirstPlayerTurn}\tchanceCounter: {chanceCounter}\t chanceCounter%2: {chanceCounter % 2}\t chanceCountet%3: {chanceCounter % 3}</color>");

            if (chanceCounter % 3 == 0 && chanceCounter > 1)
                rollDiceBtn.interactable = false;
            else
                rollDiceBtn.interactable = true;

            Debug.Log($"<color=Red> isFirstPlayerTurn: {isFirstPlayerTurn}</color>");
            Image diceImage = null;
            Tuple<Sprite, int> value = null;

            audioSrc.Play();
            while (rollDuration > 0)
            {
                //diceRollAnim.Play();
                foreach (GameObject dice in diceGameObjects)
                {
                    if (Dice.removedDices.Contains(dice))
                        continue;
                    dice.GetComponent<Button>().interactable = false;
                    diceImage = dice.GetComponent<Image>();
                    if (vsComputer && !isFirstPlayerTurn)
                    {
                        value = MainMenu.DifficultyLevel switch
                        {
                            "Easy" => AIPlayer.GetProbabilityDistribution(animSprites),
                            "Medium" => AIPlayer.GetProbabilityDistribution(animSprites),
                            "Hard" => AIPlayer.GetProbabilityDistribution(animSprites),
                            _ => AIPlayer.GetProbabilityDistribution(animSprites),
                        };
                    }
                    else
                    {
                        value = GetRandomSpriteScore();
                    }

                    diceImage.sprite = value.Item1;
                    diceImage.GetComponent<Dice>().count = value.Item2;
                }

                //---------

                rollDuration -= 3.5f;
                yield return new WaitForSeconds(0.04f);

            }
            audioSrc.Stop();

            foreach (GameObject dice in diceGameObjects)
            {
                dice.GetComponent<Image>().sprite = diceSprites[dice.GetComponent<Dice>().count - 1];
            }
            //diceRollAnim.Stop();

            for (int i = 0; i < ScoreManager.scoreCategories.Count; i++)
            {
                res = ScoreManager.CalculateScore(diceGameObjects, ScoreManager.scoreCategories[i]);
                if (isFirstPlayerTurn)
                {
                    secondPlayerBtnList[i].interactable = false;
                    if (!firstPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract)
                        continue;
                    firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                    firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = res.ToString();
                    firstPlayerBtnList[i].interactable = true;
                }
                else
                {
                    firstPlayerBtnList[i].interactable = false;
                    if (!secondPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract)
                        continue;
                    secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.white;
                    secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = res.ToString();
                    secondPlayerBtnList[i].interactable = true;
                }
            }

            foreach (GameObject dice in diceGameObjects)
            {
                dice.GetComponent<Button>().enabled = true;
                dice.GetComponent<Button>().interactable = true;
            }

            if (!isFirstPlayerTurn && vsComputer)
                AIPlayer.instance.SelectMove();

            ScoreManager.listOfScoreCaseGameobjects.Clear();
            StopCoroutine(rollingCoroutine);
            rollingCoroutine = null;
            rollDuration = 35;

            if (chanceCounter % 3 == 0)
            {
                Debug.Log("Hey thwrw");
                foreach (GameObject g in diceGameObjects)
                {
                    Dice.ResetDice();
                    yield return new WaitForSeconds(0.2f);
                    g.GetComponent<Button>().interactable = false;
                    g.GetComponent<Dice>().OnClickDice();
                }
            }
        }

        private void Update()
        {
            Time.timeScale = timeScale;
        }

        public void DisableAllButtons()
        {
            for (int i = 0; i < 11; i++)
            {
                firstPlayerBtnList[i].interactable = false;
                secondPlayerBtnList[i].interactable = false;
            }
        }

        public void OnClickAddScore()
        {
            rollDiceBtn.interactable = true;

            Debug.Log($"<color=Red>OnClickAddScore -  isFirstPlayerTurn: {isFirstPlayerTurn}\tchanceCounter: {chanceCounter}\t chanceCounter%2: {chanceCounter % 2}\t chanceCountet%3: {chanceCounter % 3}</color>");
            if (chanceCounter % 3 != 0)
            {
                Debug.Log("ChanceCounter %3 != 0");
                SwitchPlayerTurn(isFirstPlayerTurn);
            }
            else
            {
                Debug.Log("chanceCounter%3=0");
                if (chanceCounter % 2 != 0)
                {
                    Debug.Log("ChanceCounter%2!=0");
                    SwitchPlayerTurn(isFirstPlayerTurn);
                }
                else
                {
                    Debug.Log("ChanceCunter%2=0");
                    SwitchPlayerTurn(true);
                }
            }

            for (int i = 0; i < 11; i++)
            {
                {
                    secondPlayerBtnList[i].interactable = false;
                    firstPlayerBtnList[i].interactable = false;
                }
            }
        }

        public void SwitchPlayerTurn(bool isfirstPlayer)
        {
            ResetDiceAfterEachTurn();
            Invoke(nameof(ResetTextmesh), 0.1f);


            if (isfirstPlayer)
            {
                isFirstPlayerTurn = false;
                rollDicePanel.transform.DOScale(Vector3.zero, doScaleDuration)
                    .OnComplete(() =>
                    {
                        rollDiceBtn.GetComponent<Image>().color = AddScoreBtn.bluefade;
                        Debug.Log("2nd Player turn");
                        if (vsComputer)
                        {
                            blockImg.SetActive(true);
                            rollDicePanel.GetChild(1).gameObject.SetActive(false);
                            rollDicePanel.GetComponent<VerticalLayoutGroup>().reverseArrangement = true;
                            rollDicePanel.DOAnchorMin(new Vector2(0f, 1), 0.2f);
                            rollDicePanel.DOAnchorMax(new Vector2(1f, 1), 0.2f);
                            BackgroundImgObj.transform.DOMoveY(0.8f, 0.2f);
                            rollDicePanel.DOAnchorPosY(-220, 0.3f).OnComplete(() =>
                            {
                                rollDicePanel.pivot = new Vector2(0.5f, 0.5f);

                                //Roll Dice automatically if vsComputer
                                {
                                    Debug.Log("AI will Move");
                                    AIPlayer.instance.SelectMove();
                                }
                            });
                        }
                        else
                        {
                            rollDicePanel.GetComponent<VerticalLayoutGroup>().reverseArrangement = false;
                            boardParent.DOScale(Vector3.one, 0.6f).OnComplete(() => boardParent.DORotate(new Vector3(0, 0, 180), 0.4f));
                        }
                        transform.DOScale(Vector3.one, 0.5f).OnComplete(() => rollDicePanel.DOScale(Vector3.one, doScaleDuration));
                    });
                UIManager.instance.uiScreens[1].screen.GetComponent<Image>().color = new Color(0.1803922f, 0.4588236f, 0.5921569f, 1);      //blue
                Camera.main.backgroundColor = new Color(0.1803922f, 0.4588236f, 0.5921569f, 1);      //blue
            }
            else
            {
                isFirstPlayerTurn = true;
                rollDicePanel.transform.DOScale(Vector3.zero, doScaleDuration)
                    .OnComplete(() =>
                    {
                        Debug.Log("1st Player turn");

                        rollDicePanel.GetChild(1).gameObject.SetActive(true);
                        rollDiceBtn.GetComponent<Image>().color = AddScoreBtn.redfade;
                        if (vsComputer)
                        {
                            rollDicePanel.GetComponent<VerticalLayoutGroup>().reverseArrangement = false;
                            rollDicePanel.DOAnchorMin(new Vector2(0, 0), 0.2f);
                            rollDicePanel.DOAnchorMax(new Vector2(1, 0), 0.2f);
                            BackgroundImgObj.transform.DOMoveY(-0.6f, 0.2f);
                            rollDicePanel.DOAnchorPosY(250, 0.3f).OnComplete(() =>
                            {
                                rollDicePanel.pivot = new Vector2(0.5f, 0.5f);

                            });
                        }
                        else
                        {
                            rollDicePanel.GetComponent<VerticalLayoutGroup>().reverseArrangement = false;
                            boardParent.DOScale(Vector3.one, 0.6f).OnComplete(() => boardParent.DORotate(new Vector3(0, 0, 0), 0.4f));
                        }
                        transform.DOScale(Vector3.one, 0.5f).OnComplete(() => rollDicePanel.DOScale(Vector3.one, doScaleDuration));

                        blockImg.SetActive(false);
                    });
                UIManager.instance.uiScreens[1].screen.GetComponent<Image>().color = new Color(0.854902f, 0.4627451f, 0.4666667f, 1);   //red
                Camera.main.backgroundColor = new Color(0.854902f, 0.4627451f, 0.4666667f, 1);   //red
            }
            chanceCounter = 0;
        }

        void ResetDiceAfterEachTurn()
        {
            Dice.clickCount = 0;

            foreach (GameObject dice in diceGameObjects)
            {
                dice.GetComponent<Button>().enabled = false;
                dice.GetComponent<Button>().interactable = true;
                dice.GetComponent<Image>().sprite = DefaultDiceSprite;
                dice.transform.GetChild(0).gameObject.SetActive(false);
                dice.GetComponent<Dice>().count = 0;
                dice.GetComponent<Dice>().clicked = false;
                Dice.removedDices.Clear();
            }

            for (int i = 0; i < 3; i++)
            {
                Transform childIMg = rollDiceBtn.transform.GetChild(3).GetChild(i);
                childIMg.GetComponent<Image>().enabled = true;
                childIMg.GetChild(0).GetComponentInChildren<Image>().color = new Color(1, 1, 1, 1f);
            }
        }

        void ResetTextmesh()
        {
            for (int i = 0; i < 11; i++)
            {

                if (firstPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract)
                {
                    firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
                    firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                }

                if (secondPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract)
                {
                    secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = new Color(0.1058824f, 0.6235294f, 0.8627451f, 1);
                    secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                }
            }
        }

        private Tuple<Sprite, int> GetRandomSpriteScore()
        {
            int randomIndex = UnityEngine.Random.Range(0, diceSprites.Count);
            return new Tuple<Sprite, int>(animSprites[randomIndex], randomIndex + 1);
        }

        public void OnClickExit()
        {
            for (int i = 0; i < 11; i++)
            {
                firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().text = "";
                firstPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = Color.red;
                secondPlayerBtnList[i].GetComponentInChildren<TextMeshProUGUI>().color = new Color(0.1058824f, 0.6235294f, 0.8627451f, 1);
                firstPlayerBtnList[i].interactable = false;
                secondPlayerBtnList[i].interactable = false;
                secondPlayerBtnList[i].GetComponent<Image>().color = new Color(0.1058824f, 0.6235294f, 0.8627452f, 1);
                firstPlayerBtnList[i].GetComponent<Image>().color = new Color(1, 0, 0, 1);
                firstPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract = true;
                secondPlayerBtnList[i].GetComponent<AddScoreBtn>().canInteract = true;
            }

            Dice.clickCount = 0;
            counterTxt.GetComponentInChildren<TextMeshProUGUI>().text = "";
            counterTxt.SetActive(false);
            vsComputer = false;
            AddScoreBtn.firstPlayerScore = 0;
            AddScoreBtn.secondPlayerScore = 0;
            boardParent.DORotate(Vector3.zero, 0);
            audioSrc.Stop();
            rollDiceBtn.interactable = true;
            chanceCounter = 0;
            AddScoreBtn.count = 0;
            StopAllCoroutines();
            rollingCoroutine = null;
            blockImg.SetActive(false);
            scoreText.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "0";
            scoreText.transform.GetChild(2).GetComponent<TextMeshProUGUI>().text = "0";
            ResetTextmesh();
            ResetDiceAfterEachTurn();

            UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.MainMenuScreen, false);
            //rollDicePanel.DOScale(0,0);
            rollDicePanel.localScale = Vector3.zero;
            rollDicePanel.GetChild(1).gameObject.SetActive(true);
            SceneManager.LoadScene(0);
        }

        public void ShowResult()
        {
            DOTween.KillAll(true);
            Time.timeScale = 0;
            StopAllCoroutines();
            rollingCoroutine = null;
            audioSrc.mute = true;
            UIManager.instance.ShowScreen(UIScreens_ENM.resultScreen, false);
        }

        private void OnApplicationPause(bool pause)
        {
            Time.timeScale = 0;
        }

        private void OnApplicationFocus(bool focus)
        {
            Time.timeScale = 1;
        }

        public void LoadMainMenu()
        {
            SceneManager.LoadScene(0);
        }
    }
}
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class AIPlayer : MonoBehaviour
{
    public static AIPlayer instance;

    [SerializeField] TextMeshProUGUI redTmp;    //tmp to submit AI player score to as text
    private void Start()
    {
        if (instance == null)
            instance = this;
    }


    [SerializeField] List<int> p = new(6);
    public Tuple<Sprite, int> GetProbabilityDistribution(List<Sprite> spriteList)
    {
        switch (MainMenu.DifficultyLevel)
        {
            case "Easy":
                p[0] = Random.Range(0, 3);
                p[1] = Random.Range(4, 6);
                p[2] = Random.Range(2, 5);
                for (int i = 3; i <= 5; i++)
                {
                    p[i] = Random.Range(0, 6);
                }
                break;
            case "Medium":
                p[0] = Random.Range(0, 3);
                p[1] = Random.Range(0, 6);
                p[2] = Random.Range(1, 4);
                p[3] = Random.Range(1, 5);
                p[4] = Random.Range(0, 4);
                p[5] = Random.Range(2, 5);
                break;
            case "Hard":
                p[0] = Random.Range(3, 6);
                p[1] = Random.Range(2, 5);
                p[2] = Random.Range(3, 6);
                p[3] = Random.Range(2, 6);
                p[4] = Random.Range(0, 6);
                p[5] = Random.Range(1, 5);
                break;
        }

        p.OrderBy(x => Guid.NewGuid()).ToList();

        int r = p[Random.Range(0, p.Count)];
        //Debug.Log($"Sprite List Count: {spriteList.Count} \t int List Count: {p.Count} \t randomelement: {r}");
        return new Tuple<Sprite, int>(spriteList[r], r + 1);
    }


    public void SelectMove()
    {
        threeOfAKindL.Clear();
        fourOfAKindL.Clear();
        threeAndTwoKindL.Clear();
        sequenceL.Clear();
        AllSameL.Clear();        

        if (DiceRollBtn.rollingCoroutine == null)
        {
            Debug.Log("rollingDiceCoroutine was null");
            DiceRollBtn.instance.RollDice();
        }
        else
        {
            Debug.Log($"Selecting the move \t RollDiceCoroutine wasn't null");

            //When MakeTheMove(True,...............) then other parameters doesn't matter
            Debug.Log("Selecting difficulty");
            List<int> l = new List<int> { 1, 2, 3, 4, 6, 7, 8, 15,13 };
            switch (MainMenu.DifficultyLevel)
            {
                case "Easy":
                    Debug.Log("Level Easy");
                    if (l[Random.Range(0,l.Count-1)] % 2 == 0)
                        MakeTheMove(true);
                    else
                        MakeTheMove(false);
                    break;
                case "Medium":
                    Debug.Log("Level Medium");
                        MakeTheMove(false);
                    break;
                case "Hard":
                    Debug.Log("Level Hard");                    
                        MakeTheMove(false);
                    break;
                default:
                    MakeTheMove(false);
                    break;
            }
        }
    }

    void MakeTheMove(bool SubmitSCore)
    {
        StartCoroutine(Move(SubmitSCore));
    }

    [SerializeField] List<Dice> dicesToSelect = new();
    IEnumerator Move(bool _SubmitSCore)
    {
        dicesToSelect.Clear();
        Debug.Log($"Space to make AI move");
        Debug.Log("Making the move");


        if (!DiceRollBtn.instance.isFirstPlayerTurn)
        {
            bool rollDice = true;
            if (_SubmitSCore)
            {
                rollDice = false;
                Debug.Log("Submitting the score = OnCLickAddScoreBtn");
                //yield return new WaitForSeconds(0.2f);

                //int maxScore = 0;
                //int currScore=0;
                //for(int i = 0; i < 11; i++)
                //{
                    
                //AddScoreBtn btToSub = DiceRollBtn.instance.secondPlayerBtnList[i].GetComponent<AddScoreBtn>();
                  //  currScore = int.Parse( btToSub.GetComponentInChildren<TextMeshProUGUI>().text);
                    //if (currScore > maxScore)
                      //  maxScore = currScore;
                //}


                var (btnToSubmit, maxValue) = DiceRollBtn.instance.secondPlayerBtnList
                    .Where(bt => bt.GetComponent<AddScoreBtn>().canInteract)
                    .Select(bt => (bt.GetComponent<AddScoreBtn>(), int.Parse(bt.GetComponentInChildren<TextMeshProUGUI>().text)))
                    
                    .Aggregate((maxItem, currentItem) => (currentItem.Item2 > maxItem.Item2) && (currentItem.Item1.canInteract) ? currentItem : maxItem);

                Debug.Log("Max value from all addscore buttons : " + maxValue + "\t" + btnToSubmit.name + "\t" + btnToSubmit.GetComponentInChildren<TextMeshProUGUI>().text);
                if (btnToSubmit.canInteract)
                {
                    yield return new WaitForSeconds(0.2f)
;                    Debug.Log("submitting score");
                    yield return new WaitForSeconds(0.2f);
                    btnToSubmit.OnClickAddScoreBtn(redTmp);
                    yield return new WaitForSeconds(0.8f);
                    DiceRollBtn.instance.OnClickAddScore();
                    values.Clear();
                    threeOfAKindL.Clear();
                    fourOfAKindL.Clear();
                    threeAndTwoKindL.Clear();
                    AllSameL.Clear();
                    sequenceL.Clear();
                }

                yield return new WaitForSeconds(0.5f);
            }
            else    //OnClick Dice and Roll Again
            {
                rollDice = true;
                        int t = 0;
                Debug.Log("false sent       onClick dice and roll again");
                if (DiceRollBtn.chanceCounter % 3 == 0)
                {
                    rollDice = false;
                    Debug.Log($"chanceCounter %3 ==0 so submitting the score");
                    StartCoroutine(Move(true));
                }
                else
                {
                    rollDice = true;
                    Debug.Log("Select the dices to hold");
                    yield return new WaitForSeconds(0.2f);

                    if (DiceRollBtn.chanceCounter >= 1 && DiceRollBtn.chanceCounter % 3 != 0)
                    {
                       
                    var (btnToSubmit, maxValue) = DiceRollBtn.instance.secondPlayerBtnList
                        .Where(bt => bt.GetComponent<AddScoreBtn>().canInteract)
                        .Select(bt => (bt.GetComponent<AddScoreBtn>(), int.Parse(bt.GetComponentInChildren<TextMeshProUGUI>().text)))
                        .Aggregate((maxItem, currentItem) => (currentItem.Item2 > maxItem.Item2) && (currentItem.Item1.canInteract) ? currentItem : maxItem);

                        Debug.Log("chanceCounter >1 && turn now switched");
                        CheckConditions(values);

                        {
                            rollDice = true;
                            yield return new WaitForSeconds(0.2f);
                            switch (MainMenu.DifficultyLevel)
                            {
                                case "Easy":
                                    Debug.Log($"<color=Green>Easy</color>");
                                    if (maxValue >= 10)
                                    {
                                        Debug.Log("<color=Blue> Rolled Dice but still submitting score</color> + maxValue : " + maxValue);
                                        rollDice = false;
                                        MakeTheMove(true);
                                        break;                                        
                                    }
                                    else
                                    {
                                        if (AllSameL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting AllSameList</color>");
                                            dicesToSelect.AddRange(AllSameL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                        }
                                        else if (fourOfAKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting fourOFAkindLIst</color>");
                                            dicesToSelect.AddRange(fourOfAKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                        }
                                        else
                                        {
                                        dCountToRoll = Random.Range(0, 4);
                                        for(int i = 0; i < dCountToRoll; i++)
                                        {
                                            int randomDice = Random.Range(0, DiceRollBtn.instance.diceGameObjects.Count);
                                            if (!dicesToSelect.Contains(DiceRollBtn.instance.diceGameObjects[randomDice].GetComponent<Dice>()))
                                            {
                                                dicesToSelect.Add(DiceRollBtn.instance.diceGameObjects[randomDice].GetComponent<Dice>());
                                            }

                                        }
                                        }
                                        dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                    }
                                    break;
                                case "Medium":
                                    Debug.Log($"<color=Green>Medium</color>");

                                    if (maxValue >= 20)
                                    {
                                        Debug.Log("<color=Blue> Rolled Dice but still submitting score</color> + maxValue : " + maxValue);
                                        rollDice = false;
                                        MakeTheMove(true);
                                    }
                                    else
                                    {

                                        if (threeOfAKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting ThreeOfAKindList</color>");
                                            dicesToSelect.AddRange(threeOfAKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (fourOfAKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting fourOFAkindLIst</color>");
                                            dicesToSelect.AddRange(fourOfAKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (threeAndTwoKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting threeAndTwoOfAKindList</color>");
                                            dicesToSelect.AddRange(threeAndTwoKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (sequenceL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting SequenceList</color>");
                                            dicesToSelect.AddRange(sequenceL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (AllSameL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting AllSameList</color>");
                                            dicesToSelect.AddRange(AllSameL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else
                                        {
                                            Debug.Log("<color=Green> selecting no List matched</color>");
                                            dicesToSelect.Take(Random.Range(0, Random.Range(1, dicesToSelect.Count))).Where(dice=>!dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                        }
                                        //else
                                          //  dicesToSelect.Take(Random.Range(0, Random.Range(1, dicesToSelect.Count))).ToList().ForEach(dice => dice.OnClickDice());
                                    }
                                    break;
                                case "Hard":
                                    Debug.Log($"<color=Green>Hard</color>");

                                    if (maxValue >= 25)
                                    {
                                        Debug.Log("<color=Blue> Rolled Dice but still submitting score</color>+ maxValue : " + maxValue);
                                        rollDice = false;
                                        MakeTheMove(true);
                                    }
                                    else
                                    {
                                        if (threeOfAKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting threeOfAKindList</color>");
                                            dicesToSelect.AddRange(threeOfAKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (fourOfAKindL.Count > 2)
                                        {
                                            Debug.Log("<color=Green> selecting fourOfAKindList</color>");
                                            dicesToSelect.AddRange(fourOfAKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (threeAndTwoKindL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting threeAndTwoOfAKindList</color>");
                                            dicesToSelect.AddRange(threeAndTwoKindL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (sequenceL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting SequenceList</color>");
                                            dicesToSelect.AddRange(sequenceL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());

                                        }
                                        else if (AllSameL.Count > 1)
                                        {
                                            Debug.Log("<color=Green> selecting AllSameList</color>");
                                            dicesToSelect.AddRange(AllSameL.Except(dicesToSelect));
                                            dicesToSelect.Where(dice => !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                        }
                                        else
                                        {
                                            Debug.Log("<color=Green> selecting no List matched</color>");
                                            dicesToSelect.Take(Random.Range(0, Random.Range(1, dicesToSelect.Count))).Where(dice=> !dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                    else
                    {
                        Debug.Log("something wrong");
                        dicesToSelect.AddRange(DiceRollBtn.instance.diceGameObjects.Select(obj => obj.GetComponent<Dice>()).Where(Dice=>!dicesToSelect.Contains(Dice)));
                        dicesToSelect.Take(Random.Range(0, Random.Range(1, dicesToSelect.Count))).Where(dice=>!dice.clicked).ToList().ForEach(dice => dice.OnClickDice());
                    }
                    yield return new WaitForSeconds(0.2f);
                    Debug.Log("dice selected now Rolling Dice Again" + rollDice);
                }

                foreach (GameObject g in DiceRollBtn.instance.diceGameObjects)
                    if (g.GetComponent<Dice>().clicked)
                        t++;

                if (t == 5)
                {
                    Debug.Log("t==5?");
                    rollDice = false;
                    MakeTheMove(true);
                    t = 0;
                }
                if (rollDice)
                    {
                        Debug.Log("dice---");
                        //yield return new WaitForSeconds(0.2f);
                        DiceRollBtn.instance.RollDice();
                        rollDice = true;
                    }
            }//---
        }
        else
            Debug.Log("first person move");
    }    

    //[SerializeField] List<List<Dice>> finalList = new();
    [SerializeField] List<Dice> threeOfAKindL = new();
    [SerializeField] List<Dice> fourOfAKindL = new();
    [SerializeField] List<Dice> threeAndTwoKindL = new();
    [SerializeField] List<Dice> sequenceL = new();
    [SerializeField] List<Dice> AllSameL = new();
    [SerializeField] List<Dice> ObjList = new();
    [SerializeField] int dCountToRoll = 0;
    [SerializeField] List<int> values = new();
    void CheckConditions(List<int> previousList = null)
    {
        values = new();

        foreach (GameObject g in DiceRollBtn.instance.diceGameObjects)
        {
            //  if(!values.Contains(g.GetComponent<Dice>().count))
                values.Add(g.GetComponent<Dice>().count);
        }

        for (int i = 0; i < 5; i++)
        {
            ObjList[i] = DiceRollBtn.instance.diceGameObjects[i].GetComponent<Dice>();
        }

        foreach (Dice g in ObjList)
        {

            int a = 3, s = 3, t = 2, f = 3;
            //AllSame
            switch (MainMenu.DifficultyLevel)
            {
                case "Easy":
                    a = 3;
                    s = 3;
                    t = 3;
                    f = 3;                    
                    break;

                case "Medium":
                    a = 3;
                    s = 2;
                    t = 2;
                    f = 2;
                    break;

                case "Hard":
                    a = 2;
                    s = 3;
                    t = 2;
                    f = 3;
                    break;

            }

            var groups = values.GroupBy(x => g.count);
            bool hasTriple = groups.Any(y => y.Count() >= t);
            bool hasDouble = groups.Any(x => x.Count() == t-1);

            if (values.Any(x => values.Count(v => v == x) >= a))
            {
                AllSameL.Add(g);
                Debug.Log("AllSame Checkcondition");
            }

            //sequence
            if (values.Distinct().Count() <= values.Count() - s && (values.OrderBy(x => x).Zip(values.OrderBy(x => x).Skip(1), (a, b) => b - a).All(diff => diff == 1)))
            {
                sequenceL.Add(g);
                Debug.Log("Sequence Checkcondition");
            }

            //threeOfAKind
            if (values.Any(x => values.Count(v => v == g.count) >= t))
            {
                threeOfAKindL.Add(g);
                Debug.Log("ThreeOfAKind Checkcondition");
            }

            //fourofAKind
            if (values.Any(x => values.Count(v => v == g.count) >= f))
            {
                fourOfAKindL.Add(g);
                Debug.Log("FourOfAKind Checkcondition");
            }

            //threeAndTwo
            if (hasTriple && hasDouble)
            {
                threeAndTwoKindL.Add(g);
                Debug.Log("ThreeAndTwo Checkcondition");
            }
        }
    }

}


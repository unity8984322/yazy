using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AddScoreBtn : MonoBehaviour
{
    public bool isButtonScoreZero;
    public bool canInteract = true;
    public static int firstPlayerScore;
    public static int secondPlayerScore;

    public static Color bluefade = new Color(0.1058824f, 0.6235294f, 0.8627452f,0.5f);
    public static Color redfade = new Color(1, 0, 0, 0.5f);
    public static int count;
    public void OnClickAddScoreBtn(TextMeshProUGUI gb)
    {
        int scoreToAdd = 0;
        count++;
        this.canInteract = false;

        Transform effectClone = Instantiate(DiceRollBtn.instance.addScoreEffect, this.transform.position, Quaternion.identity);
        effectClone.DOScale(0.9f, 0.3f).OnComplete(() => effectClone.DOScale(0,0.2f).OnComplete(()=> Destroy(effectClone.gameObject,0.3f)));

        scoreToAdd = int.Parse(this.GetComponentInChildren<TextMeshProUGUI>().text);
        GetComponentInChildren<TextMeshProUGUI>().color = new Color(0.1960784f, 0.1960784f, 0.1960784f, 1);
        DiceRollBtn.instance.ActionTodisablebuttons.Invoke();
        Debug.Log("Score To Add: " + scoreToAdd);

        if (scoreToAdd == 0)
            isButtonScoreZero = true;
        else
            isButtonScoreZero = false;

        if (gb.name == "BlueScore")
        {
            Debug.Log($"ScoreToAdd: {scoreToAdd} \t firstPlayerScore: {firstPlayerScore} \t Btntext: {GetComponentInChildren<TextMeshProUGUI>().text} ");
            GetComponent<Image>().color = bluefade;
            firstPlayerScore += scoreToAdd;
            gb.text = firstPlayerScore.ToString();
        }

        else if (gb.name == "RedScore")
        {
            Debug.Log($"ScoreToAdd: {scoreToAdd} \t firstPlayerScore: {secondPlayerScore} \t Btntext: {GetComponentInChildren<TextMeshProUGUI>().text} ");
            GetComponent<Image>().color = redfade;
            secondPlayerScore += scoreToAdd;
            gb.text = secondPlayerScore.ToString();
        }

        if (count ==22)
        {
            StartCoroutine(CheckGameWin());
        }
    }

    IEnumerator CheckGameWin()
    {

        yield return new WaitForSeconds(0.3f);

        if (firstPlayerScore > secondPlayerScore)
        {
            DiceRollBtn.hasFirstPlayerWon = true;
        }
        else
        {

            DiceRollBtn.hasFirstPlayerWon = false;
        }
        DiceRollBtn.instance.ShowResult();

        firstPlayerScore = 0;
        secondPlayerScore = 0;
    }
}

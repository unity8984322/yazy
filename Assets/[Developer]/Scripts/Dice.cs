using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Dice : MonoBehaviour
{
    public static List<GameObject> removedDices = new();
    public bool clicked;
    public int count;
    public static int clickCount=0;
    private void OnEnable()
    {
        GetComponent<Button>().onClick.AddListener(OnClickDice);
    }

    private void OnDisable()
    {
        GetComponent<Button>().onClick.RemoveListener(OnClickDice);
    }

    public void OnClickDice()
    {
        if (!removedDices.Contains(gameObject) && !clicked)
        {
            clickCount++;
            Debug.Log("Dice.cs Dice Clicked");
            clicked = true;
            removedDices.Add(gameObject);
            transform.GetChild(0).gameObject.SetActive(true);
        }
        else 
        {
            clickCount--;
            Debug.Log("Dice.cs Dice unclicked");
            clicked = false;
            transform.GetChild(0).gameObject.SetActive(false);
            removedDices.Remove(gameObject);
        }
        Debug.Log($"<color=Blue> ChanceCounter {DiceRollBtn.chanceCounter} \t clickCount: {clickCount}</color>");

        if (DiceRollBtn.chanceCounter != 3)
        {
            if (clickCount >= 5)
                DiceRollBtn.instance.rollDiceBtn.interactable = false;
            else
                DiceRollBtn.instance.rollDiceBtn.interactable = true;

        }
        else if (DiceRollBtn.chanceCounter == 3)
            DiceRollBtn.instance.rollDiceBtn.interactable = false;
    }

    public static void ResetDice()
    {

        foreach(GameObject g in DiceRollBtn.instance.diceGameObjects) 
        {
            if (!removedDices.Contains(g))
                g.GetComponent<Dice>().OnClickDice();
        }
    }
}

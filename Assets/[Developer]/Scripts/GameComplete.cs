using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
public class GameComplete : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI blueWinTxt;
    [SerializeField] TextMeshProUGUI redWinTxt;
    [SerializeField] Image bgImage;
    
    private void OnEnable()
    {
        Time.timeScale = 0;
        blueWinTxt.transform.DOScale(0, 0);        
        redWinTxt.transform.DOScale(0, 0);        

        if (DiceRollBtn.hasFirstPlayerWon)
        {
            blueWinTxt.gameObject.SetActive(true);
            blueWinTxt.transform.DOScale(Vector3.one, 0.2f);
            bgImage.color = new Color(0.06304733f, 0.1345049f, 0.4339623f, 0.7f);
        }
        else
        {
            redWinTxt.gameObject.SetActive(true);
            redWinTxt.transform.DOScale(Vector3.one, 0.2f);
            bgImage.color = new Color(0.675f, 0.06274509f, 0.0901776f, 0.7f);
        }


    }

}

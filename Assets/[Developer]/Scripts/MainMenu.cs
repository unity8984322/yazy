using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class MainMenu : MonoBehaviour
{
    public static string DifficultyLevel="Easy";
    
    public void OnClickVsFriend()
    {
        UIManager.instance.ShowNextScreenAndHideAllOtherScreen(UIScreens_ENM.gamePlayScreen,false);
        DiceRollBtn.instance.isFirstPlayerTurn = (Random.Range(1,3)%2==0) ? true : false;
        //if (DiceRollBtn.instance.isFirstPlayerTurn)
        {
            DiceRollBtn.instance.counterTxt.SetActive(true);
            StartCoroutine(nameof(CounterTxt));
            //DiceRollBtn.instance.blockImg.SetActive(false);
            DiceRollBtn.instance.audioSrc.mute = false;
            Debug.Log($"IsFirstPlayerTUrn  :  {DiceRollBtn.instance.isFirstPlayerTurn}");
            DiceRollBtn.instance.SwitchPlayerTurn(DiceRollBtn.instance.isFirstPlayerTurn);
        }
    }

    IEnumerator CounterTxt()
    {
        TextMeshProUGUI text = DiceRollBtn.instance.counterTxt.GetComponentInChildren<TextMeshProUGUI>();
        text.transform.DOScale(1.3f,0.1f);
        yield return new WaitForSeconds(0.3f);
        text.text = "3";
        text.transform.DOScale(1f, 0.1f).OnComplete(()=> text.transform.DOScale(1.3f, 0.1f));
        yield return new WaitForSeconds(1);
        text.text = "2";
        text.transform.DOScale(1f, 0.1f).OnComplete(() => text.transform.DOScale(1.3f, 0.1f));
        yield return new WaitForSeconds(1);
        text.text = "1";
        text.transform.DOScale(1f, 0.1f).OnComplete(() => text.transform.DOScale(1.3f, 0.1f));
        yield return new WaitForSeconds(1);
        //text.transform.DOScale(1f, 0.1f).OnComplete(() => text.transform.DOScale(1.1f, 0.1f));
        DiceRollBtn.instance.counterTxt.SetActive(false);
    }

    public void OnDrpoDownValueChanged(TextMeshProUGUI tmp)
    {
        DifficultyLevel = tmp.text;
        Debug.Log($"Selected Difficulty : {DifficultyLevel}");
    }

    public void OnClickPlay()
    {        
        Time.timeScale = 1;
        if (DifficultyLevel != string.Empty && DifficultyLevel != "")
        {
            DiceRollBtn.instance.vsComputer = true;
            OnClickVsFriend();
        }

    }

    public void OnClickExitGame()
    {
        Debug.Log("Quit");
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;
using System;

public class PopUp : MonoBehaviour
{
    [SerializeField] string msgToShow;
    [SerializeField] Transform objToInstantiate;
    [SerializeField] Transform parentToInstantitae;

    public void OnClickPopUp()
    {
        GameObject[] destroy = GameObject.FindGameObjectsWithTag("ObjClone");

        foreach (GameObject g in destroy)
        {
            g.transform.DOScale(0,0.2f).OnComplete(()=> DestroyImmediate(g));            
        }

        Vector3 position;
        Quaternion rotation;
        if (DiceRollBtn.instance.isFirstPlayerTurn)
        {
            rotation = new Quaternion(transform.rotation.x, transform.rotation.y, 0, 0);
            position = new Vector3(1.3f, 0, 0);
        }
        else
        {
            rotation = rotation = new Quaternion(transform.rotation.x, transform.rotation.y, 180, 0);
            position = new Vector3(-1.3f, 0,0);
        }

        Transform clone =  Instantiate(objToInstantiate,transform.position + position ,rotation,parentToInstantitae);
        clone.GetComponentInChildren<TextMeshProUGUI>().text = msgToShow;
        clone.transform.DOScale(new Vector2(1, 1),0.2f).OnComplete(()=>
        {
            clone.DOScale(1,0.5f).OnComplete(()=>clone.DOScale(0, 0.2f).OnComplete(() => Destroy(clone.gameObject, 1.2f))); 
        });
        
    }
}

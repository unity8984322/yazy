using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class ScoreManager
{
    public static List<string> scoreCategories = new() { "1", "2", "3", "4", "5", "6", "threeOfAKind", "fourOfAKind", "3and2OfAKind", "sequence", "allSame" };

    public static List<List<GameObject>> listOfScoreCaseGameobjects = new();
    public static int CalculateScore(List<GameObject> diceGameObjects, string scoreCase)
    {
        int score = 0;

        List<int> diceValues = new();
        
        foreach (GameObject dice in diceGameObjects)
        {
            diceValues.Add(dice.GetComponent<Dice>().count);
            //Debug.Log($"diceValues: {dice.GetComponent<Dice>().count}");
        }
        //Debug.Log("diceValues.Count " + diceValues.Count);

        List<GameObject> tempList = new();
        tempList.Clear();
        listOfScoreCaseGameobjects.Clear();

        if (scoreCase.Length < 6)
        {
            if (int.Parse(scoreCase) < 7)
            {
                int count = diceValues.Count(x => x == int.Parse(scoreCase)); // Count occurrences of scoreCase in diceValues
                
                
                //foreach(GameObject g in diceGameObjects)
                //{
                  //  int c = g.GetComponent<Dice>().count;
                    //Debug.Log($"count: {c}");
                  //  if (diceValues.Contains(c) && !tempList.Contains(g))
                  //      tempList.Add(g);
                //}

               // if(!listOfScoreCaseGameobjects.Contains(tempList))
               //     listOfScoreCaseGameobjects.Add(tempList);

                //foreach (GameObject i in tempList)
                  //  Debug.Log($"Count: {i.GetComponent<Dice>().count}");
                score += count * int.Parse(scoreCase);
            }
        }
        if (scoreCase == "threeOfAKind")
        {       
            if (diceValues.Any(x => diceValues.Count(v => v == x) >= 3))
            {
                    
                Debug.Log("in threeOfAKind");
                int threeOfAKind = diceValues.Sum();
                score += threeOfAKind;
            }
        }
        if (scoreCase == "fourOfAKind")
        {
            if (diceValues.Any(x => diceValues.Count(v => v == x) >= 4))
            {
                Debug.Log("in fourOfAKind");
                int fourOfAKind = diceValues.Sum();
                score += fourOfAKind;
            }
        }
        if (scoreCase == "3and2OfAKind")
        {
            var groups = diceValues.GroupBy(x => x);
            bool hasTriple = groups.Any(y => y.Count() >= 3);
            bool hasDouble = groups.Any(x => x.Count() == 2);
            if(hasDouble && hasTriple)
            {
                Debug.Log("in 3and2ofAKind");
                score += 25;
            }
        }
        if (scoreCase == "sequence")
        {
            if (diceValues.Distinct().Count() == diceValues.Count() && (diceValues.OrderBy(x => x).Zip(diceValues.OrderBy(x => x).Skip(1), (a, b) => b - a).All(diff => diff == 1)))
            {
                Debug.Log("in sequence");
                score += 40;
            }
        }
        if (scoreCase == "allSame")
        {
            if (diceValues.Any(x => diceValues.Count(v => v == x) == 5))
            {
                Debug.Log("in allSame");
                score += 50;
            }
        }
        
        //Debug.Log($"listOfScoreCaseGameobject.Count {listOfScoreCaseGameobjects.Count}");
        return score;
    }

}
